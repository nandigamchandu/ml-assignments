# Py Filling: Lists, Tuples, Dictionaries, and Sets
#1) Python has two other sequence structures: tuples and lists.
#2) Unlike strings, the elements can be of different types.
#3) Tuples are immutable.
empty_list = [ ]
weekdays = ['Monday', 3, 'Wednesday', 'Thursday', 1, 2]
print(weekdays)
another_empty_list = list()
print(another_empty_list)
#4) Convert Other Data Types to Lists with list()
word = list('chandu')
print(word)
#5) convert tuple to list
a_tuple = ('ready', 'fire', 'aim')
word = list(a_tuple)
print(word)
#6) Get an Item by Using [ offset ]
weekdays = ['Monday', 3, 'Wednesday', 'Thursday', 1, 2]
for i in range(len(weekdays)):
    print(weekdays[i])

for i in range(len(weekdays)):
    print(weekdays[-i])
#7) list of lists
birds = ['hummingbird', 'finch']
animals = ['dog', 'cat', 'rat', 'horse']
extinct_birds = ['dodo', 'passenger pigeon', 'Norwegian Blue']
list1 = [birds,animals,extinct_birds]
print(list1) 
print(list1[1][1])
print(list1[0])
list1[1][1] = 'big cat'
print(list1[1])

#8) Add an Item to the End with append()
numbers = []
numbers.append('whole numbers')
numbers.append('natural numbers')
numbers.append('real numbers')
numbers.append('complex numbers')
print(numbers)

#9) Combine Lists by Using extend() or +=
print(list1)
print(numbers)
print(list1 + numbers)
list1.extend(numbers)
print(list1)

#10) Add an Item by Offset with insert()
x = [0, 1, 2, 4, 6, 7]
x.insert(3,3)
x.insert(10,8)
print(x)
x.insert(5, 5)
print(x)

#11) Delete an Item by Offset with del

del x[-1]
del x[5]
del x[1]
print(x)
del x[3]
print(x)

#12)  Delete an Item by Value with remove()

x= [1,2,3,4,5,6,7,8,9]
print(x)
x.remove(3)
x.remove(7)
x.remove(6)
print(x)

#13) Get an Item by Offset and Delete It by Using pop()

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo']
x0 = marxes.pop()
x1 = marxes.pop(1)
print(x0 ,x1, marxes) 

#14) Find an Item’s Offset by Value with index()

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo', 'Groucho']
print(marxes)
x0 = marxes.index('Groucho')
x1 = marxes.index('Chico')
x2 = marxes.index('Harpo')
x3 = marxes.index('Zeppo')
print(x0,x1,x2,x3)

#15) Test for a Value with in

print('Groucho' in marxes)
print('ramppo' in marxes)

#16) Count Occurrences of a Value by Using count()
marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo', 'Groucho']
print(marxes.count('Groucho'))
print(marxes.count('Zeppo'))

#17) Reorder Items with sort()

x = [1,6,2,9,7,8,5,3,4,0]
x1 = sorted(x)
print(x)
print(x1)

x.sort()
print(x)

x.sort(reverse=True)
print(x)

#18) Get Length by Using len()

x = ["ch", 'an', 'du', "pr", "as"]
print(len(x))

#19) Assign with =, Copy with copy()

a = [0, 1, 2, 3, 4]
b = a
b[0] = 'chandu'
print(b)
print(a)
# The list copy() function
# The list() conversion function
# The list slice [:]
a = [0, 1, 2, 3]
b = a.copy()
c = a[:]
d = list(a)
b[0] = 'chandu'
c[0] = 'prasad'
d[0] = 'nandigam'
print(d)
print(c)
print(b)
print(a)

#20) tuple

tuple1 = 'nandigam', 'chandu', 'prasad'
print(tuple1)
#21) tuple unpacting
a,b,c = tuple1
print(b)
#22) You can use tuples to exchange values in one statement without using a temporary 
# variable
print(a,b)
a, b = b, a 
print(a, b)
# 23) The tuple() conversion function makes tuples from other things
x = ['prasad', 'nandigam', 'chandu']
y = tuple(x)
print(x)
print(y)
# 24)  dictionaries

empty_dist = { }
print(empty_dist)

data = {'first name' : 'chandu',
'last name' : 'nandigam',
'phone no' : 9912021371}
print(data)

# 25) Convert by Using dict()
data = [['first name' , 'prasad'], ['last name', 'nandigam'], ['phone no', 9912021371]]
data1 = dict(data)
print(data1)
data = [('first name', 'prasad'), ('last name', 'nandigam'), ('phone no', 9912021371)]
data2 = dict(data)
print(data2)
data = ['ab', 'cd', 'ef']
data3 = dict(data)

#26) add or change an item by [key]
data = {'first name' : 'chandu',
'last name' : 'nandigam',
'phone no' : 9912021371}
print(data)
data['first name'] = 'prasad'
print(data)

data['middle name'] = 'chandu'

print(data)

# 27) combine dictionaries with update() function
a = {'a': 1, 'b' : 2, 'c' : 3, 'd' : 4}
b = {'e' : 5, 'f' : 6, 'g' :7, 'h' : 8}
a.update(b)
print(a)

#28) delete an item by key with del

del a['g']
del a['h']
print(a)
a.clear()
print(a)


#29) test for key by using "in"
a = {'a': 1, 'b' : 2, 'c' : 3, 'd' : 4}
print('a' in a)
print('f' in a)

#30) get the items by [key]
print(a['c'])
print(a["d"])
print(a.get('a'))
print(a.get('f', 'not their'))

#31) get all keys by keys(), values by values(), key-value pairs by items()  
print(a.keys())
print(list(a.keys()))
print(a.values())
print(list(a.values()))
print(a.items())
print(list(a.items()))


#32) assign with =, copy with copy()
a = {'a': 1, 'b' : 2, 'c' : 3, 'd' : 4}
b = a
b['a'] = 26
print(b)
print(a)

a = {'a': 1, 'b' : 2, 'c' : 3, 'd' : 4}
b = a.copy()
b['a'] = 26
print(b)
print(a)

#37) sets

a = set()
print(a)
a = {}
print(a)

a = set(list('letter'))
print(a)
print({9,9,1,2,0,2,1,3,7,1})
print(set('letter'))
print(set( {'apple': 'red', 'orange': 'orange', 'cherry': 'red'} ))

#38) Test for Value by Using in

drinks = {'martini': {'vodka', 'vermouth'},
'black russian': {'vodka', 'kahlua'},
'white russian': {'cream', 'kahlua', 'vodka'},
'manhattan': {'rye', 'vermouth', 'bitters'},
'screwdriver': {'orange juice', 'vodka'}
}

for name, content in drinks.items():
    if 'vodka' in content:
        print(name)

print('\n')
#39) Combinations and Operators
for name, content in drinks.items():
    if content & {'vodka', 'kahlua'}:
        print(name)
print('\n')
for name, contents in drinks.items():
    if 'vodka' in contents and not contents & {'vermouth', 'cream'}:
        print(name)

