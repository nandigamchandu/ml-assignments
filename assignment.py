1) map function  input: function, coll
------
def my_map(f):
    def mapped(lst):
        result = []
        for i in lst:
            result.append(f(i))
        return result
    return mapped

def power (n):
    def num (x):
        j = x
        for i in range(n - 1):
            j = x * j
        return j
    return num

square = power(2)
cube = power(3)

Output
------------------------------
a) my_map(square)([7, 2, 6, 4, 5, 1, 2]) = output: [49, 4, 36, 16, 25, 1, 4]
b) my_map(cube)([7, 2, 6, 4, 5, 1, 2])   = output: [343, 8, 216, 64, 125, 1, 8]

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
2) filter function input: pred, coll
-------
def my_filter(pred):
    def filtered(lst):
        result = []
        for i in lst:
            if pred(i) == True:
                result.append(i)
        return result
    return filtered

def is_even(n):
    if (n % 2) == 0:
        return True
    else:
        return False

def is_odd(n):
    if n % 2 != 0:
        return True
    else:
        return False

def is_neg(n):
    if n < 0:
        return True
    else:
        return False

Output
-------------------------------
a) my_filter(even)([1, 2, 3, 5, 6, 7])          = output: [2, 6]
b) my_filter(is_odd)([9, 12, 15, 8, 9, 10, 66]) = output: [9, 15, 9]
c) my_filter(is_neg)([9, 0, -1, -8, 9, 10, -66])= output: [-1, -8, -66]

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
3) sort function input: coll
------------
def my_sort (lst):
    temp = 0
    for i in range(len(lst) - 1):
      for j in range(i + 1, len(lst)):
        if lst[i] > lst[j]:
            temp = lst[j]
            lst[j] = lst[i]
            lst[i] = temp
    return lst

Output:
---------------------------------
a) my_sort([1, 5, 9, 2, 22, 100, 6, 3, 9, 2, 9, 77, 6, -1, 0]) = output: [-1, 0, 1, 2, 2, 3, 5, 6, 6, 9, 9, 9, 22, 77, 100]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
4) concat function input: coll, coll
----------
def concat(lst1, lst2):
    result = []
    for i in lst1:
        result.append(i)
    for i in lst2:
        result.append(i)
    return result

Output:
----------------------------------
a) concat([1, 2, 3, 4],[5, 6, 7, 8, 9]) = output: [1, 2, 3, 4, 5, 6, 7, 8, 9]
b) concat([1, 2, 3, 4],"chandu")        = output: [1, 2, 3, 4, 'c', 'h', 'a', 'n', 'd', 'u']
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
5) cycle function input: n, coll
---------
def cycle(n, lst):
    result = []
    l = len(lst)
    count  = 0
    while count < n:
        result.append(lst[count % l])
        count +=1
    return result

Output:
----------------------------------
a) cycle(7, [1, 2, 3]) = output: [1, 2, 3, 1, 2, 3, 1]
b) cycle(1, [1, 2, 3]) = output: [1]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
6) partition function 
-----------
def partition(n, lst, step = None , pad = []):
    result = []
    if step == None:
        step = n
    if step > n:
        l = len(lst) // step + 1
    else:
        l = len(lst) // step
    for i in range(l):
        if len(lst[i * step : i * step + n]) == n:
            result.append(lst[i * step : i * step + n])
        else:
            if len(pad) != 0 and len(lst[i * step : i * step + n])!=0:
                temp = concat(lst[i * step : i * step + n], pad)
                result.append(temp[ : n])
    return result
Output:
--------------------------------------
a) partition(3, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 6)   = output: [[0, 1, 2], [6, 7, 8]]
b) partition(3, [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 5, ["a"]) = output: [[1, 2, 3], [6, 7, 8], [11, 12, 13]]
c) partition(2, [1, 2, 3, 4, 5], 3, ["a"])                       = output: [[1, 2], [4, 5]]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
7) partition-all function[1, 2, 3, 5, 6, 2]
-----------
def partition_all(n,lst,step = None):
    result = []
    if step == None:
        step = n
    for i in range(len(lst) // step + 1):
        if len(lst[i * step : i * step + n]) != 0:
            result.append(lst[i * step : i * step + n])
    return result

Output:
---------------------------------------
a) partition_all(3, [0, 1, 2, 3, 9, 10, 11],1) = output: [[0, 1, 2], [1, 2, 3], [2, 3, 9], [3, 9, 10], [9, 10, 11], [10, 11], [11]]
b) partition_all(3, [0, 1, 2, 3, 9, 10, 11])   = output: [[0, 1, 2], [3, 9, 10], [11]]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
8) partition-by function
--------------
def partition_by(pred, lst):
    temp = 0
    result = []
    for i in range(len(lst)):
        if pred(lst[i]) and len(lst[i:]) > 1 :
            if not pred(lst[i+1]):
                result.append(lst[temp : i+1])
                temp = i+1
        elif not pred(lst[i]) and len(lst[i:]) >1:
            if pred(lst[i+1]):
                result.append(lst[temp : i+1])
                temp = i+1
        else:
            result.append(lst[temp:])
    return result

Output:
----------------------------------------
a) partition_by(is_even, [2, 4, 6, 7, 8, 9])   = output: [[2, 4, 6], [7], [8], [9]]
b) partition_by(is_neg, [2, 4, -6, -7, 8, -9]) = output: [[2, 4], [-6, -7], [8], [-9]]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
9) drop function
-------------------
def drop (n, lst):
    if n >= 0:
        return lst[n : ]
    else:
        return []

Output:
-----------------------------------------
a) drop(3, [1,2,3,4,5,6]) = output: [3, 4, 5, 6]
b) drop(0, [1,2,3,4,5,6]) = output: [1, 2, 3, 4, 5, 6]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
10) take function
-----------------
def take(n, lst):
    if n > 0:
        return lst[ : n]
    else:
        return []

Output:
------------------------------------------
a) take(10 , [1,2,3,4,5,6,7,8]) = output: [1, 2, 3, 4, 5, 6, 7, 8]
b) take(0 , [1,2,3,4,5,6,7,8])  = output: []
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
11) drop-while function
--------------
def drop_while(pred, lst):
    for i in range(len(lst)):
        if pred(lst[i]):
            if not pred(lst[i+1]):
                return lst[i+1 : ]
        else:
            return lst

Output:
---------------------------------------
a) drop_while(is_even, [2, 4, 5, 6, 7])  = output: [5, 6, 7]
b) drop_while(is_neg,[-1, -7, 5, 9, 10]) = output: [5, 9, 10]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
12) take-while function
-----------------
def take_while(pred, lst):
    for i in range(len(lst)):
        if pred(lst[i]):
            if not pred(lst[i+1]):
                return lst[ : i +1]
        else:
            return []

Output:
-------------------------------------
a) take_while(is_even, [2, 4, 6, 7, 8, 9])    = output: [2, 4, 6]
b) take_while(is_even, [1, 2, 4, 6, 7, 8, 9]) = output: []
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
13) split-at function 
------------------
def split_at(n, lst):
    result = []
    result.append(take(n, lst))
    result.append(drop(n + 1, lst))
    return result

Output:
----------------------------------------
a) split_at(20,[1,2,3,4,5,6,7,8,9]) = output: [[1, 2, 3, 4, 5, 6, 7, 8, 9], []]
b) split_at(5,[1,2,3,4,5,6,7,8,9])  = output: [[1, 2, 3, 4, 5], [6, 7, 8, 9]]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
14) split-with function
-----------------
def split_with(pred,lst):
    result = []
    result.append(take_while(pred,lst))
    result.append(drop_while(pred, lst))
    return result

Output:
-----------------------
a) split_with(is_even,[2,4,5,6,7,8])   = output: [[2, 4], [5, 6, 7, 8]]
b) split_with(is_even,[1,2,4,5,6,7,8]) = output: [[], [1, 2, 4, 5, 6, 7, 8]]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
15) dedupe function
-----------------
def dedupe(lst):
    result = []
    for i in range(len(lst) - 1):
        if lst[i] != lst[i + 1]:
            result.append(lst[i])
        if i + 1 == len(lst) - 1:
            result.append(lst[i+1])
    return result

Output:
-------------------------
a) dedupe([1, 2, 2, 2, 3, 3 , 5, 5, 6, 2, 2, 2, 2]) output: [1, 2, 3, 5, 6, 2]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
16) interleave function
------------------
def min(lst1, lst2):
    if len(lst1) < len(lst2):
        return len(lst1)
    else:
        return len(lst2)

def interleave(lst1, lst2):
    result = []
    n = min(lst1, lst2)
    i = 0
    while(i < n):
        result.append(lst1[i])
        result.append(lst2[i])
        i = i + 1
    return result

Output:
---------------------------------------------------------
a) interleave([1, 2, 3, 4], "chandu") = output: [1, 'c', 2, 'h', 3, 'a', 4, 'n']
b) interleave([1, 2, 3, 4], [9,10])   = output: [1, 9, 2, 10]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
17) interpose function
---------------------
def interpose(lst1):
    def interposed(lst2):
        result = []
        n = len(lst2)
        i = 0
        while i < n:
            result.append(lst2[i])
            if i < n-1:
                result.append(lst1)
            i += 1
        return result
    return interposed

Output:
---------------------------------------------------------
a) interpose([1, 2, 3, 5])([1, 2, 3])                  = output: [1, [1, 2, 3, 5], 2, [1, 2, 3, 5], 3]
b) interpose([1, 2])(["chandu", "prasad", "nandigam"]) = output: ['chandu', [1, 2], 'prasad', [1, 2], 'nandigam']
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
18) flatten function
----------------
def flatten(lst):
    result = []
    for i in lst:
        if type(i) == list:
            for j in i:
                result.append(j)
        else:
            result.append(i)
    
    return result

Output:
-------------------------------------------------------
a) flatten([[1, 2], 3, [2, 3, 4], 8])  = output: [1, 2, 3, 2, 3, 4, 8]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
19) distinct function
----------------
def distinct(lst):
    result = []
    for i in lst:
        count = 0
        for j in result:
            if i == j:
                count += 1
        if count == 0:
            result.append(i)
    return result

Output:
---------------------------------------------------------------
a) distinct([1, 2, 5, 4, 1, 2, 3, 6, 1, 3]) = output: [1, 2, 5, 4, 3, 6]
b) distinct("chandunandigam")               = output: ['c', 'h', 'a', 'n', 'd', 'u', 'i', 'g', 'm']
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
20) mapcat function
--------------
def mapcat(f, lst):
    result = []
    map1 = my_map(f)
    for i in lst:
        result = concat(result,map1(i))
    return result

Output:
----------------------------------------------------------------
a) mapcat(square, [[1, 2],[3, 4],[5, 6]])  = output: [1, 4, 9, 16, 25, 36]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
21) reduce function
---------------
def reduce(bif, init, lst):
    result = init
    for e in lst:
        result = bif(result, e)
    return result

def plus(x,y):
    return x + y

Output:
-----------------------------------------------------------------
a) reduce(plus, 0, [1, 2, 3, 4, 6]) = output: 16
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
22) sort-by function
----------------
def sort_by(keyfn ,lst):
    temp = 0
    for i in range(len(lst) - 1):
        for j in range(i+1, len(lst)):
            if keyfn(lst[i]) > keyfn(lst[j]):
                temp = lst[j]
                lst[j] = lst[i]
                lst[i] = temp
    return lst

def count(str):
    return len(str)

Output:
---------------------------------------------------------------
a) sort_by(count, ["a","aaa","aa","b","bbbb"]) = output: ['a', 'b', 'aa', 'aaa', 'bbbb']
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
23) reverse function
------------
def reverse(lst):
    return lst[-1::-1]

Output:
--------------------------------------------------------------
a) reverse([1, 2, 3, 4, 5, 6]) = output: reverse([1, 2, 3, 4, 5, 6])
b) reverse("ChaNduNandIgaM")   = output: 'MagIdnaNudNahC'
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++





















