# coding: utf-8
# modules, packages, and programs
print('this interactive snippet works')
# command-line arguments
%run test.py
%run test.py tra la la
# modues and the import statment
#A module is just a file of Python code
# import a module
import report
import report
description = report.get_description()
print("today's weather:", description)
%run weatherman.py
# You should consider importing from outside the function if the imported code might be used in more than one place, and from inside if you know its use will be limited. Some people prefer to put all their imports at the top of the file
def get_description():
    import random
    possibilities = ['rain', 'snow', 'sleet', 'fog', 'sun', 'who knows']
    return random.choice(possibilities)
get_description()
get_description()
import random
def get_description():
    possibilities = ['rain', 'snow', 'sleet', 'fog', 'sun', 'who knows']
    return random.choice(possibilities)
def get_description():
    possibilities = ['rain', 'snow', 'sleet', 'fog', 'sun', 'who knows']
    return random.choice(possibilities)
get_description()
get_description()
# import a module with another name
import report as wr 
description = wr.get_description()
print("today's weather:", description)
# import only what you want from a module
from report import get_description
description = get_description()
print("today's weather:", description)
# import it as do_it
from report import get_description as do_it
print("today's weather:", description)
description = do_it()
print("today's weather:", description)
# module search path
import sys
for place in sys.path:
    print(place)
    
#packages
from sources import daily, weekly
def forecast():
    'fake daily forecast'
    return 'like yesterday'

def forecast():
    """ Fake weekly forecast"""
    return ['snow', 'more snow', 'sleet', 'freezing rain', 'rain', 'fog', 'hail']
from sources import daily, weekly
print("Daily forecast:", daily.forecast())
print("Weekly forecast:")
for number, outlook in enumerate(weekly.forecast(), 1):
    print(number, outlook)
%run weather.py
# handle missing keys with setdefault() and defaultdict() 
words = ['apple', 'bat', 'bar', 'atom', 'book']

        
        
by_letter = {}
for word in words:
    letter = word[0]
    if letter not in by_letter:
        by_letter[letter] = [word]
    else:
        by_letter[letter].append(word)
        
        
by_letter
for word in words:
    letter = word[0]
    by_letter.setdefault(letter, []).append(word)
    
by_letter
by_letter = {}
for word in words:
    letter = word[0]
    by_letter.setdefault(letter, []).append(word)
    
by_letter
from collections import defaultdict
by_letter = defaultdict(list)
for word in words:
    by_letter[word[0]].append(word)
    
by_letter
#count items with counter()
from collections import Counter
breakfast = ['spam', 'spam', 'eggs', 'spam']
breakfast_counter = Counter(breakfast)
breakfast_counter
breakfast_counter.most_common()
# The most_common() function returns all elements in descending order, or just the top count elements if given a count:
breakfast_counter.most_common(1)
breakfast_counter
lunch = ['eggs','eggs', 'bacon']
lunch_cunter = Counter(lunch)
lunch_cunter
breakfast_counter + lunch_cunter
breakfast_counter - lunch_cunter
lunch_cunter - breakfast_counter
breakfast_counter & lunch_cunter
breakfast_counter | lunch_cunter
# order by key with orderedDist()
quotes = {}
quotes = {}
quotes = {"moe" : " a wise guy, huh?", 'larry' : 'ow!', 'Curly' : 'Nyuk nyuk!',}
for stooge in quotes:
    print(stooge)
    
from collections import OrderedDict
quotes = OrderedDict(quotes)
quotes
# stack + queue == deque
def palindrome(word):
    from collections import deque
    dq = deque(word)
    while len(dq) > 1:
        if dq.popleft() != dq.pop():
            return False
    return True
palindrome('a')
palindrome('racecar')
palindrome('')
palindrome('radar')
palindrome('halibut')
def another_palindrome(word):
    return word == word[::-1]
another_palindrome('radar')
another_palindrome('halibut')
# iterate over code strutures with itertools
import itertools
for item in itertools.chain([1, 2], ['a', 'b']):
    print(item)
    
# chain() runs through its arguments as though they were a single iterable:
# cycle() is an infinite iterator, cycling through its arguments:
import itertools
i = 0
a1 = itertools.cycle([1, 2])

