import population_density as pop
import pandas as pd
import unittest as ut

class TestClosestReviewer(ut.TestCase):
    def test_pop_den_2010(self):
        x = pd.Series({ ('DC', 'District of Columbia', 605125.0, 68): 1.0}) 
        y = pop.population_density_rank('state-population.csv', 'state-areas.csv', 'state-abbrevs.csv', 2010).head(1)
        self.assertSequenceEqual(list((x.index, x.values)), list((y.index, y.values)))

if __name__ == '__main__':
    ut.main()

#output


#Ran 1 test in 0.233s
#OK