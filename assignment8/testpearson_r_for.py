import recommendmovietest as rm 
import numpy as np
import unittest as ut 

class TestPearson_r_for(ut.TestCase):

# without missing values
    def test1(self):
        v1 = np.array([74, 66, 68, 69, 73, 70, 60, 63, 67, 70, 70, 70, 75, 62, 75])
        v2 = np.array([193, 133, 155, 147, 175, 128, 100, 128, 170, 182, 178, 118, 227, 115, 211])
        self.assertAlmostEqual(rm.pearson_r_for(v1,v2), 0.8321756)

# with missing values
    def test2(self):
        v1 = np.array([74, 66, 68, 69, 73, -1, 70, 60, 63, -1, 67, 70, 70, 70, 78, 75, 62, 75])
        v2 = np.array([193, 133, 155, 147, 175, 170, 128, 100, 128, -1, 170, 182, 178, 118, -1, 227, 115, 211])
        self.assertAlmostEqual(rm.pearson_r_for(v1,v2), 0.8321756)

# when input arrays are unequal
    def test2(self):
        v1 = np.array([74, 66, 68, 69, 73, 70, 60, 63, 67, 70, 70, 70, 75, 62, 75])
        v2 = np.array([193, 133, 155, 147, 175, 128, 100, 128, 170, 182, 178, 118, 227, 115, 211])
        self.assertAlmostEqual(rm.pearson_r_for(v1,v2), 0.8321756)

# lengths of input arrays are not equal

    def test3(self):
        v1 = np.array([74, 66, 68, 69, 73, 70, 60, 63, 67, 70, 70, 70, 75, 62, 75])
        v2 = np.array([193, 133, 155, 147, 175, 128, 100, 128, 170, 182, 178, 118, 227, 115])
        with self.assertRaises(ValueError):
            rm.pearson_r_for(v1, v2)

if __name__ == '__main__':
    ut.main()

# output

# test1 (__main__.TestPearson_r_for) ... ok
# test2 (__main__.TestPearson_r_for) ... ok
# test3 (__main__.TestPearson_r_for) ... ok
#----------------------------------------------------------------------
# Ran 3 tests in 0.002s
#OK