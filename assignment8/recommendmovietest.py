
import numpy as np

def pearson(arr1, arr2):
    arr1_standard_units = (arr1 - np.mean(arr1)) / np.std(arr1)
    arr2_standard_units = (arr2 - np.mean(arr2)) / np.std(arr2)
    return np.sum(arr1_standard_units * arr2_standard_units) / np.size(arr1)



def normalise(arr, lowerlimit, upperlimit):
    return (arr - lowerlimit) / (upperlimit - lowerlimit)



def remove_missing(arr1, arr2):
    temp = ~np.any([np.isnan(arr1), np.isnan(arr2)], axis = 0 )
    return arr1[temp], arr2[temp]


def pearson_r_for(ar1, ar2):
    arr1, arr2 = remove_missing(ar1, ar2)
    arr1_standard_units = (arr1 - np.mean(arr1)) / np.std(arr1)
    arr2_standard_units = (arr2 - np.mean(arr2)) / np.std(arr2)
    return np.sum(arr1_standard_units * arr2_standard_units) / np.size(arr1)

