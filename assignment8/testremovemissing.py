import recommendmovietest as rm 
import numpy as np
import unittest as ut

class TestRemoveMissing(ut.TestCase):

# arr1 has miss values 
    def test1(self):
        arr1 = np.array([1, 2, 'nan', 3, 'nan', 4],dtype='f2')
        arr2 = np.array([1, 2, 3, 4, 5, 6], dtype='f2')
        expect = ([1, 2, 3, 4], [1, 2, 4, 6] )
        actual1, actual2 = rm.remove_missing(arr1, arr2)
        self.assertSequenceEqual((list(actual1), list(actual2)), expect)

# arr2 has miss values
    def test2(self):
        arr1 = np.array([1, 2, 3, 4, 5, 6 ],dtype='f2')
        arr2 = np.array([1, 2, 'nan', 3, 4, 'nan'],dtype= 'f2')
        expect = ([1, 2, 4, 5], [1, 2, 3, 4])
        actual1 , actual2 = rm.remove_missing(arr1, arr2)
        self.assertSequenceEqual((list(actual1), list(actual2)), expect)

# arr1 has all miss values
    def test3(self):
        arr1 = np.array(['nan', 'nan', 'nan', 'nan', 'nan', 'nan'],dtype='f2')
        arr2 = np.array([1, 2, 3, 4, 5, 6],dtype='f2')
        expect = ([], [])
        actual1, actual2 = rm.remove_missing(arr1,arr2)
        self.assertSequenceEqual((list(actual1), list(actual2)), expect)

# arr2 has all miss values
    def test4(self):
        arr1 = np.array([1, 2, 3, 4, 5, 6], dtype='f2')
        arr2 = np.array(['nan', 'nan', 'nan', 'nan', 'nan', 'nan'],dtype='f2')
        expect = ([], [])
        actual1, actual2 = rm.remove_missing(arr1,arr2)
        self.assertSequenceEqual((list(actual1), list(actual2)), expect)
    
# Both arr1, arr2 has miss values
    def test5(self):
        arr1 = np.array([1, 2, 'nan', 3, 4, 'nan'], dtype='f2')
        arr2 = np.array([1, 'nan', 2, 3, 4, 'nan'], dtype='f2')
        expect = ([1, 3, 4], [1, 3, 4])
        actual1, actual2 = rm.remove_missing(arr1, arr2)
        self.assertSequenceEqual((list(actual1), list(actual2)), expect)

# arr1 and arr2 are empty
    def test6(self):
        arr1 = np.array([])
        arr2 = np.array([])
        expect = ([], [])
        actual1, actual2 = rm.remove_missing(arr1, arr2)
        self.assertSequenceEqual((list(actual1), list(actual2)), expect)

# input arrays are unequal
    def test7(self):
        arr1 = np.array([1, 2, 'nan', 5],dtype ='f2')
        arr2 = np.array(['nan', 2, 3, 4, 5, 6], dtype = 'f2')
        with self.assertRaises(ValueError):
            rm.remove_missing(arr1, arr2)
    
    def test8(self):
        arr1 = np.array([1,2, 3, 4, 5])
        arr2 = np.array([1, 2, 3, 4, 5])
        expect = ([1, 2, 3, 4, 5], [1, 2, 3, 4, 5])
        actual1, actual2 = rm.remove_missing(arr1, arr2)
        self.assertSequenceEqual((list(actual1), list(actual2)), expect)

if __name__ == '__main__':
    ut.main()

#output

#test1 (__main__.TestRemoveMissing) ... ok
#test2 (__main__.TestRemoveMissing) ... ok
#test3 (__main__.TestRemoveMissing) ... ok
#test4 (__main__.TestRemoveMissing) ... ok
#test5 (__main__.TestRemoveMissing) ... ok
#test6 (__main__.TestRemoveMissing) ... ok
#test7 (__main__.TestRemoveMissing) ... ok
#test8 (__main__.TestRemoveMissing) ... ok
#----------------------------------------------------------------------
#Ran 8 tests in 0.002s
#OK
