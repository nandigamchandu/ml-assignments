import numpy as np

# reading data from file 

def read_files(movieratingsfile):
    movieratings = np.loadtxt(movieratingsfile,
                              delimiter= ',',
                              skiprows = 1,)
    return movieratings

# normalise function

def normalise(arr, lowerlimit, upperlimit):
    return (arr - lowerlimit) / (upperlimit - lowerlimit)

# removing missing value function

def remove_missing(arr1, arr2):
    temp = ~np.any([arr1 < 0, arr2 < 0], axis = 0 )
    return arr1[temp], arr2[temp]

# normaliseratings function

def normalise_ratings(ratings):
    moviereviewers = np.array([('imdb', 0, 10), ('rt', 0, 10), ('rv', 0, 4), 
                  ('re', 0, 4), ('mc', 0, 100), ('tg', 0, 5), 
                  ('ac', 0, 5)])
    x = ratings[1:]
    y = moviereviewers[:, 1].astype('f2')
    z = moviereviewers[:, 2].astype('f2')
    return np.apply_along_axis(normalise, 0, x, y, z)

#  pearson correlation function

def pearson_correlation(ar1):
    def pearson_correlation1(ar2):
        arr1, arr2 = remove_missing(ar1, ar2)
        arr1_standard_units = (arr1 - np.mean(arr1)) / np.std(arr1)
        arr2_standard_units = (arr2 - np.mean(arr2)) / np.std(arr2)
        return np.sum(arr1_standard_units * arr2_standard_units) / np.size(arr1)
    return pearson_correlation1  

# function to get matrix of corrlation 

def correlation_matrix(movieratingsfile):
    ratings = read_files(movieratingsfile).T
    normaliseratings = normalise_ratings(ratings)
    temp = np.apply_along_axis(pearson_correlation, 1, normaliseratings)
    return [np.apply_along_axis(i, 1, normaliseratings) for i in temp]


print(correlation_matrix('movie_ratings.csv')) 

# output


#[array([ 1.        ,  0.79369474,  0.77184799,  0.49074981,  0.70006521, 0.5818662 ,  0.7621764 ]),
# array([ 0.79369474,  1.        ,  0.74862341,  0.7352014 ,  0.97840264, 0.69532301,  0.8608502 ]), 
# array([ 0.77184799,  0.74862341,  1.        ,  0.35737401,  0.71583721, 0.64972212,  0.67180041]), 
# array([ 0.49074981,  0.7352014 ,  0.35737401,  1.        ,  0.72074969, 0.66110736,  0.64406119]),
# array([ 0.70006521,  0.97840264,  0.71583721,  0.72074969,  1.        , 0.70995553,  0.79448682]), 
# array([ 0.5818662 ,  0.69532301,  0.64972212,  0.66110736,  0.70995553, 1.        ,  0.79372539]),
# array([ 0.7621764 ,  0.8608502 ,  0.67180041,  0.64406119,  0.79448682, 0.79372539,  1.        ])]