import numpy as np
moviereviewers = np.array([('imdb', 0, 10), ('rt', 0, 10), ('rv', 0, 4), 
                  ('re', 0, 4), ('mc', 0, 100), ('tg', 0, 5), 
                  ('ac', 0, 5)])


# reading data from file 

def readfiles(movieratingsfile):
    movieratings = np.loadtxt(movieratingsfile,
                              delimiter= ',',
                              skiprows = 1,)
    return movieratings

# normalise function

def normalise(arr, lowerlimit, upperlimit):
    lowerlimit, upperlimit = float(lowerlimit), float(upperlimit)
    return (arr - lowerlimit) / (upperlimit - lowerlimit)

# removing missing value function

def removemissing(arr1, arr2):
    temp = ~np.any([arr1 < 0, arr2 < 0], axis = 0 )
    return arr1[temp], arr2[temp]

# normaliseratings function

def normaliseratings(ratings):
    return np.array(list(map(normalise,ratings[1 :], moviereviewers[:,1], moviereviewers[:,2] )))

#  pearson correlation function

def pearsoncorrelation(ar1, ar2):
    arr1, arr2 = removemissing(ar1, ar2)
    arr1_standard_units = (arr1 - np.mean(arr1)) / np.std(arr1)
    arr2_standard_units = (arr2 - np.mean(arr2)) / np.std(arr2)
    return np.sum(arr1_standard_units * arr2_standard_units) / np.size(arr1)  

# function to get matrix of corrlation 

def correlationmatrix(movieratingsfile):
    ratings = readfiles('movie_ratings.csv').T
    normalise_ratings = normaliseratings(ratings)
    return np.array([pearsoncorrelation(i,j) for i in normalise_ratings 
                     for j in normalise_ratings]).reshape(len(normalise_ratings), len(normalise_ratings))
print(correlationmatrix('movie_ratings.csv')) 

# output

#[[ 1.          0.79369474  0.77184799  0.49074981  0.70006521  0.5818662  0.7621764 ]
# [ 0.79369474  1.          0.74862341  0.7352014   0.97840264  0.69532301 0.8608502 ]
# [ 0.77184799  0.74862341  1.          0.35737401  0.71583721  0.64972212 0.67180041]
# [ 0.49074981  0.7352014   0.35737401  1.          0.72074969  0.66110736 0.64406119]
# [ 0.70006521  0.97840264  0.71583721  0.72074969  1.          0.70995553 0.79448682]
# [ 0.5818662   0.69532301  0.64972212  0.66110736  0.70995553  1.         0.79372539]
# [ 0.7621764   0.8608502   0.67180041  0.64406119  0.79448682  0.79372539 1.        ]]