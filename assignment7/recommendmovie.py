import numpy as np

movie_reviewers = [('imdb', 0, 10), ('rt', 0, 10), ('rv', 0, 4), 
                  ('re', 0, 4), ('mc', 0, 100), ('tg', 0, 5), 
                  ('ac', 0, 5)]

# function to reading data from .csv files

def readingfiles(movieratingsfile, movienamesfile):
    movieratings = np.loadtxt(movieratingsfile, 
                     dtype = {'names': ('ID', 'IMDB', 'RT', 'RV', 'RE', 'MC', 'TG', 'AC'), 
                              'formats': ('S2', 'f2', 'f2', 'f2', 'f2', 'f2', 'f2', 'f2')}, 
                     delimiter= ',',
                     skiprows= 1, unpack=True)
    movienames = np.loadtxt(movienamesfile,
                         dtype = 'S40', 
                         delimiter= ',')
    return (movieratings, movienames)

# function to get pearson correlation coefficient between given vectors

def pearsoncorrelation(arr1, arr2):
    arr1_standard_units = (arr1 - np.mean(arr1)) / np.std(arr1)
    arr2_standard_units = (arr2 - np.mean(arr2)) / np.std(arr2)
    return np.sum(arr1_standard_units * arr2_standard_units) / np.size(arr1)

# function for normalise

def normalise(arr, lowerlimit, upperlimit):
    return (arr - lowerlimit) / (upperlimit - lowerlimit)

# function to get 'movie name' for given movie 'ID'

def getting_movie_name(movienames, id):
    for i in range(len(movienames.T[0])):
        if movienames.T[0][i] == id:
            return (movienames.T[1][i], movienames.T[2][i])

# function to find index for given 'reviewer'

def indexofreviewer(moviereviewers, reviewer):
    for i in range(len(moviereviewers)):
        if moviereviewers[i][0] == reviewer:
            return i

# converting movie ratings to dictionary

def dict_movieratings(moviereviewers, movieratings):
    mdict = {}
    for i in range(len(moviereviewers)):
        mdict[moviereviewers[i][0]] = movieratings[i + 1]
    return mdict

# function to mormalis movie ratings

def dict_nor_movieratings(moviereviewers, dictmovieratings):
    mdict = {}
    for i in range(len(moviereviewers)):
        mdict[moviereviewers[i][0] + '_n'] = normalise(dictmovieratings[moviereviewers[i][0]], moviereviewers[i][1], moviereviewers[i][2])
    return mdict

# function to remove missing values

def remove_missing(dict_nor_movieratings_, ior_, withreviewer_ ):
    names = list(dict_nor_movieratings_.keys())
    dict1=dict_nor_movieratings_.copy()
    arr1= dict1[names[ior_]][~(np.any([dict1[names[ior_]] < 0, dict1[names[withreviewer_]] < 0], axis = 0 ))]
    arr2= dict1[names[withreviewer_]][~(np.any([dict1[names[ior_]] < 0, dict1[names[withreviewer_]] < 0 ], axis = 0))]
    return arr1, arr2

# function to get correlations between given reviewer and remaining reviewer

def correlation_between_reviewers(moviereviewers, dict_nor_movieratings_, ior_):
    mdict = {}
    name = list(dict_nor_movieratings_.keys())
    for i in range(len(moviereviewers)):
        if i != ior_:
            (arr1, arr2) = remove_missing(dict_nor_movieratings_, ior_, i)
            mdict[moviereviewers[ior_][0] + '_'  + moviereviewers[i][0]] = pearsoncorrelation(arr1, arr2)
    return mdict

# function to get recommend movie

def recommend_movie(correlation, dict_ratings, movienames, ratings):
    key = list(correlation.keys())[np.argmax(list(correlation.values()))]
    value = np.max(list(correlation.values()))
    x = key.split('_')
    y = np.max(dict_ratings[x[1]])
    y1 = dict_ratings[x[1]].copy()
    y1[dict_ratings[x[0]] != -1] = 0
    if np.all(y1 == 0 ):
        y3 = 'no recommendation'
        return (key, value, y3)
    y2 = ratings[0][np.argmax(y1)]
    y3 = getting_movie_name(movienames, y2)
    return (key, value, y3)

# function to recommend movie 

def max_correlation_between_given_with_others(movieratingsfile, movienamesfile, moviereviewers, reviewer):
    ratings, names = readingfiles(movieratingsfile, movienamesfile)
    ior = indexofreviewer(moviereviewers, reviewer)
    dict_of_ratings = dict_movieratings(moviereviewers, ratings)
    dict_of_normalise_rating = dict_nor_movieratings(moviereviewers, dict_of_ratings)
    dict_of_correlation = correlation_between_reviewers(moviereviewers, dict_of_normalise_rating, ior)
    recommend_mov = recommend_movie(dict_of_correlation, dict_of_ratings, names, ratings)
    return recommend_mov

movie_reviewers = [('imdb', 0, 10), ('rt', 0, 10), ('rv', 0, 4), 
                  ('re', 0, 4), ('mc', 0, 100), ('tg', 0, 5), 
                  ('ac', 0, 5)]


max_correlation_between_given_with_others('movie_ratings.csv', 'movie_names.csv', movie_reviewers, 'imdb')
# ('imdb_rt', 0.7940848214285714, 'no recommendation')

max_correlation_between_given_with_others('movie_ratings.csv', 'movie_names.csv', movie_reviewers, 'ac')
# ('ac_rt', 0.8616071428571429, (b'HoHW', b'Hell or High Water'))

max_correlation_between_given_with_others('movie_ratings.csv', 'movie_names.csv', movie_reviewers, 'rv')
# ('rv_imdb', 0.7723214285714286, 'no recommendation')