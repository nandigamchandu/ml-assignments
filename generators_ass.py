1) map function 
-----------
def my_map(f):
    def mapped(lst):
        result = []
        for i in lst:
            yield f(i)
    return mapped

def power (n):
    def num (x):
        j = x
        for i in range(n - 1):
            j = x * j
        return j
    return num

​square = power(2)
cube = power(3)

Output:
-----------------------------------------------------------------------------
a) my_map(square)([7, 2, 6, 4, 5, 1, 2])      = output: <generator object my_map.<locals>.mapped at 0x7ff2543e0728>
b)list(my_map(square)([7, 2, 6, 4, 5, 1, 2])) = output: [49, 4, 36, 16, 25, 1, 4]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
2) filter function
----------
def my_filter(pred):
    def filtered(lst):
        result = []
        for i in lst:
            if pred(i) == True:
                yield i
    return filtered

def is_even(n):
    if (n % 2) == 0:
        return True
    else:
        return False

Output:
------------------------------------------------------------------------------
a) my_filter(is_even)([1, 2, 3, 5, 6, 7])       = output: <generator object my_filter.<locals>.filtered at 0x7ff2543e0d00>
b) list(my_filter(is_even)([1, 2, 3, 5, 6, 7])) = output: [2, 6]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
3) concat function
-----------
def concat(lst1, lst2):
    for i in lst1:
        yield i
    for i in lst2:
        yield i

Output:
-------------------------------------------------------------------------------
​a) concat(range(5),range(15,20))       = output: <generator object concat at 0x7ff2543e0e60>
b) list(concat(range(5),range(15,20))) = output: [0, 1, 2, 3, 4, 15, 16, 17, 18, 19]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
4) cycle function
----------
def cycle(n, lst):
    result = []
    l = len(lst)
    count  = 0
    while count < n:
        yield lst[count % l]
        count +=1

Output:
--------------------------------------------------------------------------------
a) cycle(7, [1, 2, 3])        = output: <generator object cycle at 0x7ff2543e07d8>
b) list(cycle(11, [1, 2, 3])) = output: [1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
5) partition function
------------
def partition(n, lst, step = None , pad = []):
    if step == None:
        step = n
    if step > n:
        l = len(lst) // step + 1
    else:
        l = len(lst) // step
    for i in range(l):
        if len(lst[i * step : i * step + n]) == n:
            yield (x for x in lst[i * step : i * step + n])
        else:
            if len(pad) != 0 and len(lst[i * step : i * step + n])!=0:
                temp = concat(lst[i * step : i * step + n], pad)
                yield (x for x in temp[ : n])

​Output:
---------------------------------------------------------------------------------
a) partition(3, list(range(1,20)))       = output: <generator object partition at 0x7ff2543e0c50>
b) list(partition(3, list(range(1,20)))) = output:

[<generator object partition.<locals>.<genexpr> at 0x7ff2543e08e0>,
 <generator object partition.<locals>.<genexpr> at 0x7ff2543e0ca8>,
 <generator object partition.<locals>.<genexpr> at 0x7ff2543e05c8>,
 <generator object partition.<locals>.<genexpr> at 0x7ff25440d048>,
 <generator object partition.<locals>.<genexpr> at 0x7ff25440d0a0>,
 <generator object partition.<locals>.<genexpr> at 0x7ff25440d0f8>]


c) for i in (partition(3, list(range(1,20)))):
      print(list(i))                     = output:

[1, 2, 3]
[4, 5, 6]
[7, 8, 9]
[10, 11, 12]
[13, 14, 15]
[16, 17, 18]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
6) partition_all function
--------------
def partition_all(n,lst,step = None):
    result = []
    if step == None:
        step = n
    for i in range(len(lst) // step + 1):
        if len(lst[i * step : i * step + n]) != 0:
            yield (x for x in lst[i * step : i * step + n])

Output:
-----------------------------------------------------------------------------------
a) partition_all(3, [0, 1, 2, 3, 9, 10, 11],1)       = output: <generator object partition_all at 0x7ff25440de08>
b) list(partition_all(3, [0, 1, 2, 3, 9, 10, 11],1)) = output:

[<generator object partition_all.<locals>.<genexpr> at 0x7ff25440db48>,
 <generator object partition_all.<locals>.<genexpr> at 0x7ff25440d888>,
 <generator object partition_all.<locals>.<genexpr> at 0x7ff25440daf0>,
 <generator object partition_all.<locals>.<genexpr> at 0x7ff25440d620>,
 <generator object partition_all.<locals>.<genexpr> at 0x7ff25440d6d0>,
 <generator object partition_all.<locals>.<genexpr> at 0x7ff25440d5c8>,
 <generator object partition_all.<locals>.<genexpr> at 0x7ff25440d7d8>]

c) for i in (partition_all(3, [0, 1, 2, 3, 9, 10, 11],1)):
      print(list(i))

[0, 1, 2]
[1, 2, 3]
[2, 3, 9]
[3, 9, 10]
[9, 10, 11]
[10, 11]
[11]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
7) partition by function
------------
def partition_by(pred, lst):
    temp = 0
    result = []
    for i in range(len(lst)):
        if pred(lst[i]) and len(lst[i:]) > 1 :
            if not pred(lst[i+1]):
                yield (x for x in lst[temp : i+1])
                temp = i+1
        elif not pred(lst[i]) and len(lst[i:]) >1:
            if pred(lst[i+1]):
                yield (x for x in lst[temp : i+1])
                temp = i+1
        else:
            yield (x for x in lst[temp:])
Output:
--------------------------------------------------------------------------------------------------
a) partition_by(is_even, [1, 7, 9, 2, 4, 9, 8, 7, 9])       = output: <generator object partition_by at 0x7ff2543acbf8>
b) list(partition_by(is_even, [1, 7, 9, 2, 4, 9, 8, 7, 9])) = output:

[<generator object partition_by.<locals>.<genexpr> at 0x7ff25440dbf8>,
 <generator object partition_by.<locals>.<genexpr> at 0x7ff25440d990>,
 <generator object partition_by.<locals>.<genexpr> at 0x7ff25440dca8>,
 <generator object partition_by.<locals>.<genexpr> at 0x7ff25440d938>,
 <generator object partition_by.<locals>.<genexpr> at 0x7ff25440dd00>]

c) for i in (partition_by(is_even, [1, 7, 9, 2, 4, 9, 8, 7, 9])):
      print(list(i))

[1, 7, 9]
[2, 4]
[9]
[8]
[7, 9]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
8) drop function
---------
def drop (n, lst):
    if n >= 0:
        for i in range(n, len(lst)):
            yield lst[i]
    else:
        for i in range(len(lst)):
            yield lst[i]
Output:
-----------------------------------------------------------------------------------------------
a) drop(3, [1,2,3,4,5,6])        = output: <generator object drop at 0x7ff2543a0830>
b) list(drop(3, [1,2,3,4,5,6]))  = output: [4, 5, 6]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
9) take function
--------
def take(n, lst):
    if n > 0:
        if n > len(lst):
            n = len(lst)
        for i in range(n):
            yield lst[i]
    else:
        return []

Output:
-----------------------------------------------------------------------------------------------
a) take(5, [1,2,3,4,5,6,7,8])              = output: <generator object take at 0x7ff25440df68>
b) print(list(take(5, [1,2,3,4,5,6,7,8]))) = output: [1, 2, 3, 4, 5]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
10) drop-while function
------------
def drop_while(pred, lst):
    for i in range(len(lst)):
        if pred(lst[i]):
            if not pred(lst[i+1]):
                for j in range(i+1, len(lst)):
                    yield lst[j]
                break
        else:
            for i in range(len(lst)):
                yield lst[i]
            break
Output:
----------------------------------------------------------------------------------------------
a) drop_while(is_even, [1, 2, 4, 5, 6, 7])              = output: <generator object drop_while at 0x7ff2543a04c0>
b) print(list(drop_while(is_even, [1, 2, 4, 5, 6, 7]))) = output: [1, 2, 4, 5, 6, 7]
c) print(list(drop_while(is_even, [2, 4, 5, 6, 7])))    = output: [5, 6, 7]
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
11) take-while function
-------------
def take_while(pred, lst):
    for i in range(len(lst)):
        if pred(lst[i]):
            if not pred(lst[i+1]):
                for j in range(i + 1):
                    yield lst[j]
                break
        else:
            return []

Output:
----------------------------------------------------------------------------------------
a) take_while(is_even, [2, 4, 6, 7, 8, 9])                 = output: <generator object take_while at 0x7ff2543a0ca8>
b) print(list(take_while(is_even, [2, 4, 6, 7, 8, 9])))    = output: [2, 4, 6]
c) take_while(is_even, [1, 2, 4, 6, 7, 8, 9])              = output: <generator object take_while at 0x7ff2543a0c50>
d) print(list(take_while(is_even, [1, 2, 4, 6, 7, 8, 9]))) = output: []
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
12) dedupe function
--------
def dedupe(lst):
    for i in range(len(lst) - 1):
        if lst[i] != lst[i + 1]:
            yield lst[i]
        if i + 1 == len(lst) - 1:
            yield lst[i+1]

Output:
-----------------------------------------------------------------------------------------
a) dedupe([1, 2, 2, 2, 3, 3 , 5, 5, 6, 2, 2, 2, 2, 3])              = output: <generator object dedupe at 0x7ff2543a03b8>
b) print(list(dedupe([1, 2, 2, 2, 3, 3 , 5, 5, 6, 2, 2, 2, 2, 3]))) = output: [1, 2, 3, 5, 6, 2, 3]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
13) interleave function
--------
def min(lst1, lst2):
    if len(lst1) < len(lst2):
        return len(lst1)
    else:
        return len(lst2)

def interleave(lst1, lst2):
    n = min(lst1, lst2)
    i = 0
    while(i < n):
        yield lst1[i]
        yield lst2[i]
        i = i + 1

Output:
----------------------------------------------------------------------------------------
a) interleave([1, 2, 3, 4], "chandu") = output: <generator object interleave at 0x7ff2543a0938>
b) print(list(interleave([1, 2, 3, 4], "chandu"))) = output: [1, 'c', 2, 'h', 3, 'a', 4, 'n']

c) for i in (interleave([1, 2, 3, 4], "chandu")):
     print(i)

1
c
2
h
3
a
4
n
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
14) interpose function
---------
def interpose(lst1, lst2):  
    n = len(lst2)
    i = 0
    while i < n:
        yield lst2[i]
        if i < n-1:
            yield lst1
        i += 1

Output:
--------------------------------------------------------------------------------------------
a) interpose(",", ["chandu", "prasad", "nandigam"]) = output: <generator object interpose at 0x7ff2543a0048>
b) for i in (interpose(",", ["chandu", "prasad", "nandigam"])):
     print(i, end='')

                                                    = output: chandu,prasad,nandigam
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
15) flatten function
---------
def flatten(lst):
    for i in lst:
        if type(i) == list:
            for j in i:
                yield j
        else:
            yield i

Output:
---------------------------------------------------------------------------------------------
a) flatten([[1, 2], 3, [2, 3, 4], 8]) = output: <generator object flatten at 0x7ff2543a0eb8>
b) for i in (flatten([[1, 2], 3, [2, 3, 4], 8])):
      print(i, end=', ')
                                     = output: 1, 2, 3, 2, 3, 4, 8, 
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
16) mapcat function
---------
def mapcat(f, lst):
    for i in lst:
        for j in i:
            yield f(j)

Output:
--------------------------------------------------------------------------------------------
a) mapcat(square, [[1, 2],[3, 4],[5, 6]]) = output: <generator object mapcat at 0x7ff2543d1410>
b) for i in (mapcat(square, [[1, 2],[3, 4],[5, 6]])):
      print(i, end= ', ')
                                          = output: 1, 4, 9, 16, 25, 36, 
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
17) reverse function
----------
def reverse(lst):
    for i in range(1, len(lst) + 1):
        yield lst[-i]

Output:
--------------------------------------------------------------------------------------------
a) reverse([1, 2, 3, 4, 5, 6]) = output: <generator object reverse at 0x7ff25440d150>
b) for i in (reverse([1, 2, 3, 4, 5, 6])):
      print(i, end = ', ')
                               = output: 6, 5, 4, 3, 2, 1,
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
18) split-at function
------------ 
def split_at(n, lst):
    yield (x for x in (take(n, lst)))
    yield (x for x in (drop(n + 1, lst)))

Output:
-------------------------------------------------------------------------------------------
a) split_at(5,[1,2,3,4,5,6,7,8,9]) = output: <generator object split_at at 0x7ff2543d1360>
b) list(split_at(5,[1,2,3,4,5,6,7,8,9]))

[<generator object split_at.<locals>.<genexpr> at 0x7ff2543d1570>,
 <generator object split_at.<locals>.<genexpr> at 0x7ff2543d11a8>]

c) for i in (split_at(5,[1,2,3,4,5,6,7,8,9])):
      print(list(i))

[1, 2, 3, 4, 5]
[7, 8, 9]
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
19) split-with function
------------
def split_with(pred,lst):
    yield (x for x in (take_while(pred,lst)))
    yield (x for x in (drop_while(pred, lst)))

Output:
-------------------------------------------------------------------------------------------
a) list(split_with(is_even,[2,4,5,6,7,8]))

[<generator object split_with.<locals>.<genexpr> at 0x7ff2543d1150>,
 <generator object split_with.<locals>.<genexpr> at 0x7ff2543d16d0>]

b) for i in (split_with(is_even,[2,4,5,6,7,8])):
      print(list(i))

[2, 4]
[5, 6, 7, 8]


