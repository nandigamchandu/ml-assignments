#1) Data types
#2) Python is strongly typed, which means that the type of an object does not change, even
# if its value is mutable.
a = 7
b = a
print(type(a))
print(a,b)
a = 10
print(b)

#3) / carries out floating-point (decimal) division
#4) // performs integer (truncating) division

# counting digits
def no_digits(n):
    temp = n
    temp1 = n
    count = 0
    while temp != 0:
        temp //= 10
        temp1 /= 10
        count +=1
        print( temp ,  temp1, sep =', ' )
    print(n)
    return count
print(no_digits(123456789))

#5)  we can get quotient and remainder at once
print(divmod(1956,27))

#6) precedence
print(2 + 3 * 9)
print((2 + 3) * 9)

#7) Bases

print(435, 0b110110011, 0o663 , 0x1B3, sep = ',')

#8) Type Conversions
a = []
a.append(int('198'))
a.append(int(1.0e4))
a.append(int(False))
a.append(int(True))
a.append(int("-11"))
a.append(int('+92'))
a.append(int(99.089))
a.append(True + 5)
a.append(False + 6.08)
a.append(int(False + 6.08))
print(a)

#9) Python 3, long is long gone, and an int can be any size
print(10 ** 100)
print(10 ** 100 * 10 ** 100)

#10) convert to float data type
a = []
a.append(float(10))
a.append(float('1.456e6'))
a.append(float('10.0'))
a.append(float(True))
a.append(float(False))
a.append(False + 10.68)
a.append(True + 12.9 + 0.8)
print(a)

#11) string

a = []
a.append('hello world')
a.append("hello world")
a.append("hello 'chandu'")
a.append('hello "chandu"')
for i in a:
    print(i)

#12) to create multiline strings

poem = '''There was a Young Lady of Norway,
Who casually sat in a doorway;
When the door squeezed her flat,
She exclaimed, "What of that?"
This courageous Young Lady of Norway.'''
print(poem)

#13) Why would you need an empty string? Sometimes you might want to build a string from
#other strings, and you need to start with a blank slate.
bottles = 99
base = ''
base += 'current inventory: '
base += str(bottles)
print(base)

#14) Convert Data Types by Using str()
a = []
a.append(str(10))
a.append(str(19.08))
a.append(str(1.06e5))
a.append(str(True))
a.append(str(False))
print(a)

#15) Escape with \
palindrome = 'A man,\nA plan,\nA canal:\nPanama.'
print(palindrome)

print('\tabc')
print('a\tbc')
print('ab\tc')
print('abc\t')

testimony = "\"I did nothing!\" he said. \"Not that either! Or the other thing.\""
print(testimony)

#16) Combine with +
print('Release the kraken! ' + 'At once!')

a = 'Duck.'
b = a
c = 'Grey Duck!'
print(a + b + c)

#17) Duplicate with *
start = 'Na ' * 4 + '\n'
middle = 'Hey ' * 3 + '\n'
end = 'Goodbye.'
print(start + start + middle + end)

#18) Extract a Character with []
name = 'chandunandigam'
for i in range(len(name)):
    print(name[i], end= '  ')

#19) Slice with [ start : end : step ]
print(end = '\n')
print(name[0:6])
print(name[6:])
print(name[2:])
print(name[-1 : : -1])
print(name[::2])
print(name[::-1])

#19) replace string character with replace() 
x = 'chancdu'
print(x.replace('c', 'm'))

#20) replace with slice
x = 'chandu'
for i in range(len(x) + 1):
    print(x[:i] +'\t' + x[i:])

#21) split with split()
todos = 'get gloves,get mask,give cat vitamins,call ambulance'
print(todos.split(','))
print(todos.split()) #split() uses any sequence of white space characters
print(todos)

#22) Combine with join()
todos1 = todos.split()  
print(todos1)
print(', '.join(todos1))

#23) play with string
poem = '''All that doth flow we cannot liquid name
Or else would fire and water be the same;
But that is liquid which is moist and wet
Fire that property can never get.
Then 'tis not cold that doth the fire put out
But 'tis the wet that makes it die, no doubt.'''

print(poem.startswith('All'))
print(poem.endswith('doubt.'))
word = 'that'
print(poem.find(word))
print(poem.rfind(word))
print(poem.count(word))

#24) Case and Alignment
setup = 'a duck goes into a bar...'
print(setup.strip('.'))
print(setup)
setup1 = '     a duck goes into a bar...'
print(setup1.strip(' '))
print(setup1)
print(setup)
print(setup.capitalize())
print(setup.lower())
print(setup.swapcase())
print(setup.upper())
print(setup.title())





