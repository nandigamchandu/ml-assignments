import pandas as pd 
import numpy as np

# reading data from file
def read_files(movieratingsfile):
    movieratings = pd.read_csv(movieratingsfile, index_col=['ID'])
    return movieratings

# remove missing value function
def remove_missing(arr1, arr2):
    temp = ~np.any([np.isnan(arr1), np.isnan(arr2)], axis = 0 )
    return arr1[temp], arr2[temp]

# normalise function
def normalise_ratings(movieratings):
    y = pd.DataFrame({'lowerlimit' : {'IMDB': 0, 'RT': 0, 'RV': 0, 'RE': 0, 'MC': 0, 'TG': 0, 'AC': 0},
                    'upperlimit': {'IMDB': 10, 'RT': 10, 'RV': 4, 'RE': 4, 'MC': 100, 'TG': 5, 'AC': 5}}).T
    return (movieratings - y.loc['lowerlimit']) / (y.loc['upperlimit'] - y.loc['lowerlimit'])

# pearson correlation function
def pearsoncorrelation(ar1):
    def pearsoncorrelation1(ar2):
        arr1, arr2 = remove_missing(ar1, ar2)
        arr1_standard_units = (arr1 - np.mean(arr1)) / np.std(arr1)
        arr2_standard_units = (arr2 - np.mean(arr2)) / np.std(arr2)
        return np.sum(arr1_standard_units * arr2_standard_units) / np.size(arr1)
    return pearsoncorrelation1

# recommend movie function
def recommend_movie(moviefilename):
    ratings = read_files(moviefilename)
    normaliseratings = normalise_ratings(ratings)
    func = pearsoncorrelation(normaliseratings['AC'])
    correlation = normaliseratings.apply(func, axis = 0)
    correlation['AC'] = 0
    closest_reviewer = correlation.idxmax()
    return closest_reviewer, ratings[ratings['AC'].isnull()][closest_reviewer].idxmax()
recommend_movie('movie_ratings.csv')

print(recommend_movie('movie_ratings.csv'))

# ('RT', 11)