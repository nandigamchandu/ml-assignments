
def max (x,y):
    if x > y:
        return x
    else:
        return y
print(max(12,576))

def max3 (x, y, z):
    return max(max(x,y), z)
print(max3(1, 45, 999))

def max_lst (first,*args):
    for i in args:
        if first < i:
            first = i
    return first
print(max_lst(16, 77, 8, 66, 9)) 

def max_array (args):
    first = 0
    for i in args:
        if first < i:
            first = i
    return first
print(max_array([1, 88, 9, 88]))

def factorial (n):
    fact = 1
    for i in range(1, n + 1):
        fact = fact * i
    return fact
print(factorial(5))

def ncr (n, r):
    return factorial(n) / (factorial(n - r) * factorial(r))
print(ncr(3, 1))

def pascal_line (n):
    result = []
    for i in range(n + 1):
        result.append(ncr(n , i))
    return result
print(pascal_line(10))

def pascal_triangle (n):
    result = []
    for i in range(n):
        result.append(pascal_line(i))
    return result
print(pascal_triangle(9))

def my_map (f, lst):
    result = []
    for i in lst:
        result.append(f(i))
    return result

def power (x, n):
    j = x
    for i in range(n - 1):
        j = x * j
    return j
print(power(2, 3))

def num (n):
    def powe (x):
        j = x
        for i in range(n - 1):
            j = x * j
        return j
    return powe

square = num(2)

cube = num(3)

square(11)

my_map(square, [1, 2, 6, 11, 13, 4])

def my_filter(pred, lst):
    result = []
    for i in lst:
        if pred(i) == True:
            result.append(i)
    return result

def my_filter1(pred):
    def my_filtered(lst):
        result = []
        for i in lst:
            if pred(i) == True:
                result.append(i)
        return result
    return my_filtered

def even(n):
    if (n % 2) == 0:
        return True

print(even(2))

print(my_filter(even, [1, 2, 3, 5, 6, 7]))

def factors(n):
    result = []
    for i in range(1, n):
        if (n % i) == 0:
            result.append(i)
    return result
print(factors(100))

def sum1(lst):
    result = 0
    for i in lst:
        result = result + i
    return result
print(sum1([1, 2, 3, 4, 5, 6]))

def perfect_no(start, stop):
    perfectno = []
    for i in range(start, stop + 1):
        if sum1(factors(i)) == i:
            perfectno.append(i)
    return perfectno
print(perfect_no(1, 1000))

print(len(factors(1)))

def prime_no(start, stop):
    prime = []
    for i in range(start, stop + 1):
        if len(factors(i)) == 1:
            prime.append(i)
    return prime
print(prime_no(1, 50))

print(1//10)

def count_digits(n):
    count = 0
    while n != 0:
        n = n // 10
        count += 1
    return count
print(count_digits(1669))

def armstrong_no(no):
    x = no
    n = count_digits(x)
    sum1=0
    while x != 0:
        digit = x % 10
        x = x // 10
        sum1 += power(digit, n)
    if sum1 == no:
        return True
    else:
        return False
print(armstrong_no(371))

print(my_filter(armstrong_no, range(10000)))

def my_map1(f):
    def mapp(lst):
        result = []
        for i in lst:
            result.append(f(i))
        return result
    return mapp
print(my_map1(square)([1, 2, 3, 4, 5]))

def compose(f, g):
    def composer(*args):
        result = []
        for e in args:
            result.append(f(g(e)))
        return result
    return composer
print(compose(square,square)(3))

def reduce(bif, init, lst):
    result = init
    for e in lst:
        result = bif(result, e)
    return result
def reduce1(bif, init):
    def reduce2(args):
        result = init
        for e in args:
            result = bif(result, e)
        return result
    return reduce2
def plus(x,y):
    return x + y
print(reduce(plus, 0, [1, 2, 3, 4, 6]))
print(reduce1(plus,3)([1, 2, 3, 4]))

def pipe2(f,g):
    def piped (*args):
        return g(f(*args))
    return piped
def pipe(first, *rest):
    def piped2(*args):
        result = first(*args)
        for f in rest:
            result = f(result)
        return result
    return piped2

f = pipe(
my_filter1(even),
my_map1(square),
reduce1(plus, 0))

print(f([1, 2, 3, 4, 5, 6]))

f1 = pipe(
my_filter1(lambda x: x % 2 == 0),
my_map1(lambda x: x*x),
reduce1((lambda x, y: x+y), 0))

print(f1([1, 2, 3, 4, 5, 6]))

def mean (lst):
    result = 0
    for i in lst:
        result += i
    return result/(len(lst))
print(mean([1, 2, 3, 4, 5, 6]))

def my_sort (lst):
    temp = 0
    for i in range(len(lst) - 1):
      for j in range(i + 1, len(lst)):
        if lst[i] > lst[j]:
            temp = lst[j]
            lst[j] = lst[i]
            lst[i] = temp
    return lst
print(my_sort([7,1, 6, 5, 2, 2, 4]))

def median(lst):
    length = len(lst)
    lst2 = my_sort(lst)
    if length % 2 == 0:
        median1 = (lst2[(length // 2) - 1] + lst2[length // 2]) / 2
    else:
        median1 = (lst2[(length - 1) // 2]) / 2
    return median1
print(median([1, 2, 3, 4, 5, 6, ]))

def variance(lst):
    length = len(lst)
    m = mean(lst)
    sum3 = 0
    for i in lst:
        sum3 = sum3 + square(m - i)
    return sum3 / length
print(variance([1, 2, 3, 4, 5, 6, 7]))

def sqrt(x):
    return x**(1 / 2.0)
print(sqrt(81))

def standard_deviation(lst):
    return sqrt(variance(lst))
print(standard_deviation([1, 2, 3, 4, 5, 5, 6]))

def percentile(lst, p):
    lst1 = my_sort(lst)
    length = len(lst)
    return (p / 100) * (length + 1)
print(percentile([2, 2, 3, 4, 5, 5, 5, 6, 7, 8, 8, 8, 8, 8, 9, 9, 10, 11, 11, 12], 25))

def fibo(n):
    arr = [0, 1]
    for i in range(2, n):
        arr.append(arr[i - 2] + arr[i - 1])
    return arr
print(fibo(10))

def frequency(lst):
    d = {}
    for i in lst:
        d[i] = d.get(i, 0) + 1
    return d            
print(frequency([1, 1, 2, 2, 7, 8, 9, 2, 7, 6]))