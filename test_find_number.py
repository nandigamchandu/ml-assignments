import findnumber as fn
import unittest as ut

class TestIsNumber(ut.TestCase):
    def test1(self):
        self.assertEqual(fn.is_number('19,69,456'), True)

    def test2(self):
        self.assertEqual(fn.is_number('4566897'), True)

    def test3(self):
        self.assertEqual(fn.is_number('1e12'), True)

    def test4(self):
        self.assertEqual(fn.is_number('+456461'), True)

    def test5(self):
        self.assertEqual(fn.is_number('-1564566'), True)

    def test6(self):
        self.assertEqual(fn.is_number('+19,56,569'), True)

    def test7(self):
        self.assertEqual(fn.is_number('-15,15,456'), True)

    def test8(self):
        self.assertEqual(fn.is_number('+1e12'), True)

    def test9(self):
        self.assertEqual(fn.is_number('-1e12'), True)


    def test10(self):
        self.assertEqual(fn.is_number('10,00.002'), True)

    def test11(self):
        self.assertEqual(fn.is_number('1e-100'), True)

    def test12(self):
        self.assertEqual(fn.is_number('10e+10'), True)

    def test13(self):
        self.assertEqual(fn.is_number('10e+10e+10'), False)

    def test14(self):
        self.assertEqual(fn.is_number('3.000e-00'), True)

    def test15(self):
        self.assertEqual(fn.is_number('a12'), False)

    def test16(self):
        self.assertEqual(fn.is_number('10e025e'), False)

    def test17(self):
        self.assertEqual(fn.is_number('ahysjgf'), False)

if __name__ == '__main__':
    ut.main()


# output
#---------------------------------
#Ran 17 tests in 0.043s

#OK
